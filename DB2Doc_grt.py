from wb import *
import grt
from mforms import FileChooser
import mforms

ModuleInfo = DefineModule(name="DB2Doc", author="Vanzo Luca Samuele", version="1.0", description="Data Dictionary")

@ModuleInfo.plugin("vls.DB2Doc.htmlDataDictionary", caption="DBDoc: As HTML File", description="Data Dictionary as HTML", input=[wbinputs.currentCatalog()], pluginMenu="Catalog")
@ModuleInfo.export(grt.INT, grt.classes.db_Catalog)
def htmlDataDictionary(catalog):
  # Put plugin contents here
  htmlOut = ""
  filechooser = FileChooser(mforms.SaveFile)
  if filechooser.run_modal():
    htmlOut = filechooser.get_path()
    print "HTML File: %s" % (htmlOut)
  if len(htmlOut) <= 1:
    return 1

  # iterate through columns from schema
  schema = catalog.schemata[0]
  htmlFile = open(htmlOut, "w")
  print >>htmlFile, "<html><head>"
  print >>htmlFile, "<title>Data Dictionary: %s</title>" % (schema.name)
  print >>htmlFile, """<style>
    body { background-color: white; font-family: monospace; padding: 0px 20px 30px 20px; margin: 0px; }
    td,th {
      text-align:center;
      vertical-align:middle;
    }
    table {
        margin-top: 30px;
      border-collapse: collapse;
      width: 100%;
    }
    caption, th, td {
      padding: .5em 0.8em;
      border: 1px solid #004D40;
    }
    caption {
      background-color: #004D40;
        color: white;
      font-weight: bold;
      font-size: 1.1em;
        font-family: sans-serif;
      border: 0px;
        text-transform: uppercase;
        text-align: left
    }
    th {
      font-weight: bold;
      background-color:#00695C;
        color: white;
    }
    td:nth-child(odd) {
      background-color: whitesmoke;
    }
    td:nth-child(even) {
      background-color:#ececec;
    }
    td:last-child { text-align: justify; }
  </style>
  </head>
  <body>"""
  for table in schema.tables:
    print >>htmlFile, "<table><caption>Table: %s</caption>" % (table.name)
    print >>htmlFile, """<tr><td colspan=\"7\" style=\"text-align: center !important;\">Attributes</td></tr>
    <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Not Null</th>
    <th>PK</th>
    <th>FK</th>
    <th>Default</th>
    <th>Comment</th>
    </tr>"""
    for column in table.columns:
      pk = ('No', 'Yes')[bool(table.isPrimaryKeyColumn(column))]
      fk = ('No', 'Yes')[bool(table.isForeignKeyColumn(column))]
      nn = ('No', 'Yes')[bool(column.isNotNull)]
      print >>htmlFile, "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>" % (column.name,column.formattedType,nn,pk,fk,column.defaultValue,column.comment.replace("\n", "<br />"))
    print >>htmlFile, "</table></br>"
  print >>htmlFile, "</body></html>"
  return 0
